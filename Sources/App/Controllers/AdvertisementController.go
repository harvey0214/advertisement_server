package Controllers

import (
	"ad_server/Sources/App/Database"
	"ad_server/Sources/App/Environment"
	"ad_server/Sources/App/Model/Entities"
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/tencentyun/cos-go-sdk-v5"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type AdveritsementController struct{}

func (controller *AdveritsementController) Select(context *gin.Context) {
	Database.WritableDatabase.Do(func(db *gorm.DB) error {
		var ad Entities.Advertisement
		if db.First(&ad, 3).RecordNotFound() {
			return nil
		} else {
			context.JSON(http.StatusOK, ad)
			return nil
		}
	})
}

func (controller *AdveritsementController) Create(c *gin.Context) {
	title := c.PostForm("title")
	description := c.PostForm("description")
	link := c.PostForm("link")
	position := c.PostForm("position")
	status := c.PostForm("status")
	thumbnailPath := c.PostForm("thumbnailPath")
	file, fileErr := c.FormFile("image")
	src, srcErr := file.Open()


	if fileErr != nil || srcErr != nil  {
		c.JSON(http.StatusNotAcceptable, "image problem")
		return
	}

	if title == "" {
		c.JSON(http.StatusNotFound, "title is null")
		return
	}

	reader := bufio.NewReader(src)
	content, _ := ioutil.ReadAll(reader)
	encoded := base64.StdEncoding.EncodeToString(content)

	Database.WritableDatabase.Do(func(db *gorm.DB) error {
		var ad Entities.Advertisement
		ad.Title = title
		ad.Description = description
		ad.Status = status
		ad.Link = link
		ad.Position = position
		ad.ThumbnailPath = thumbnailPath

		if err := db.Create(&ad).Error; err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return err
		} else {
			var ad_thumbnail Entities.AdvertisementThumbnail
			ad_thumbnail.Ratio = "_2_1"
			ad_thumbnail.DataUrl = fmt.Sprintf("/thumbnail/advertisement/advertisementID/%d/ratio_2_1", ad.ID)
			ad_thumbnail.AdvertisementID = ad.ID

			if err := db.Create(&ad_thumbnail).Error; err != nil {
				c.JSON(http.StatusBadRequest, err.Error())
				return err
			} else {
				u, _ := url.Parse("https://pv-oa-1303879511.cos.ap-guangzhou.myqcloud.com")
				b := &cos.BaseURL{BucketURL: u}

				client := cos.NewClient(b, &http.Client{
					Transport: &cos.AuthorizationTransport{
						SecretID:  Environment.Environment.SecretID,
						SecretKey: Environment.Environment.SecretKey,
					},
				})


				f := strings.NewReader(encoded)

				_, err = client.Object.Put(context.Background(), ad_thumbnail.DataUrl, f, nil)
				if err != nil {
					c.JSON(http.StatusBadRequest, err.Error())
					return err
				}

				c.JSON(http.StatusOK, &ad)
				return nil
			}
		}

	})
}

