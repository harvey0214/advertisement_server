package Database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var WritableDatabase database

type database struct {
	dataSource string
	db *gorm.DB
}

func (engine *database) Initial(host string, port string, user string, password string, dbname string) {
	engine.dataSource = fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
}

func (engine *database) Do(handler func(db *gorm.DB) error) error {
	var err error
	engine.db, err = gorm.Open("postgres", engine.dataSource)
	if err != nil {
		panic(err)
	}
	defer engine.db.Close()
	return handler(engine.db)
}