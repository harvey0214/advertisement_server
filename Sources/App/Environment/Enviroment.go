package Environment

type env struct {
	DomainName           string
	Port                 string
	WritablePostgresHost string
	WritablePostgresPort string
	PostgresUser         string
	PostgresPassword     string
	PostgresDbName       string
	SecretID             string
	SecretKey            string
}

var Environment env
