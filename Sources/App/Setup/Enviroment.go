package Setup

import (
	"ad_server/Sources/App/Environment"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func InitialEnvironment() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file!!!!!!!!!!")
	}

	Environment.Environment.DomainName = os.Getenv("DOMAIN_NAME")
	Environment.Environment.Port = os.Getenv("PORT")
	Environment.Environment.WritablePostgresHost = os.Getenv("WRITABLE_POSTGRES_HOST")
	Environment.Environment.WritablePostgresPort = os.Getenv("WRITABLE_POSTGRES_PORT")
	Environment.Environment.PostgresUser = os.Getenv("POSTGRES_USER")
	Environment.Environment.PostgresPassword = os.Getenv("POSTGRES_PASSWORD")
	Environment.Environment.PostgresDbName = os.Getenv("POSTGRES_DB_NAME")
	Environment.Environment.SecretID = os.Getenv("SECRET_ID")
	Environment.Environment.SecretKey = os.Getenv("SECRET_KEY")

	println(Environment.Environment.Port)
}