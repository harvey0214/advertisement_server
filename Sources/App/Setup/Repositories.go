package Setup

import (
	"ad_server/Sources/App/Database"
	"github.com/jinzhu/gorm"
)

func InitialRepositories() {
	Database.WritableDatabase.Do(func(db *gorm.DB) error {
		db.AutoMigrate(
			//new(Entities.Advertisement),
		)
		return nil
	})
}
