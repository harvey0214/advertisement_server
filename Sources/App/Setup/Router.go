package Setup

import (
	"ad_server/Sources/App/Controllers"
	"ad_server/Sources/App/Environment"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log"
	"time"
)

var (
	advertisementController = Controllers.AdveritsementController{}
)


func InitialRouter() {
	router := gin.Default()

	// Set up cors
	router.Use(cors.New(cors.Config{
		AllowOriginFunc: func(origin string) bool {
			return true
		},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Accept", "Content-Type", "Access-Control-Allow-Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	//Group v1
	v1 := router.Group("v1")

	// ad
	ad := v1.Group("advertisements")
	ad.POST("",advertisementController.Create)

	err := router.Run(":" + Environment.Environment.Port)
	if err != nil {
		log.Println(err)
	}
}