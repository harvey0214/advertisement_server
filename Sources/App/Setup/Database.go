package Setup

import (
	"ad_server/Sources/App/Database"
	"ad_server/Sources/App/Environment"
	"database/sql"
	"fmt"
)

func InitialDatabase() {



	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=disable",
		Environment.Environment.WritablePostgresHost,
		Environment.Environment.WritablePostgresPort,
		Environment.Environment.PostgresUser,
		Environment.Environment.PostgresPassword))


	if err != nil {
		panic(err)
	}
	defer db.Close()


	//statement := fmt.Sprintf("SELECT EXISTS(SELECT datname FROM pg_catalog.pg_database WHERE datname = '%s')",
	//	Environment.Environment.PostgresDbName)
	//fmt.Println(statement)
	//
	//row := db.QueryRow(statement)

	//var exists bool
	//
	//err = row.Scan(&exists)
	//
	//
	//log.Println(exists)
	//
	//if err != nil {
	//	panic(err)
	//}
	//
	//if exists {
	//	log.Println("DATABASE EXIST")
	//} else {
	//	log.Println("DATABASE DOES NOT EXIST, CREATE DATABASE")
	//	//_, err = db.Exec("CREATE DATABASE " + Environment.Environment.PostgresDbName)
	//	if err != nil {
	//		panic(err)
	//	}
	//}

	Database.WritableDatabase.Initial(
		Environment.Environment.WritablePostgresHost,
		Environment.Environment.WritablePostgresPort,
		Environment.Environment.PostgresUser,
		Environment.Environment.PostgresPassword,
		Environment.Environment.PostgresDbName)
}