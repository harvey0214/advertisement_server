package Entities

import "time"

type Advertisement struct {
	ID        uint      `gorm:"primary_key"`
	CreatedAt time.Time `gorm:"column:createdAt"`
	UpdatedAt time.Time `gorm:"column:updatedAt"`
	//DeletedAt *time.Time `json:"deletedAt"`
	Title         string `json:"title"`
	Description   string `json:"description"`
	Position      string `json:"position"`
	Link          string `json:"link"`
	Status        string `json:"status"`
	ThumbnailPath string `gorm:"column:thumbnailPath"`
}

func (Advertisement) TableName() string {
	return "advertisements"
}
