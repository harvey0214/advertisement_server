package Entities

import "time"

type AdvertisementThumbnail struct {
	ID              uint      `gorm:"primary_key"`
	CreatedAt       time.Time `gorm:"column:createdAt"`
	UpdatedAt       time.Time `gorm:"column:updatedAt"`
	AdvertisementID uint      `gorm:"column:advertisement_id"`
	DataUrl         string    `gorm:"column:data_url"`
	Ratio           string
}

func (AdvertisementThumbnail) TableName() string {
	return "advertisement_encoding_thumbnail"
}