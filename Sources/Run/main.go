package main

import "ad_server/Sources/App/Setup"

func main() {
	Setup.InitialEnvironment()
	Setup.InitialDatabase()
	//Setup.InitialRepositories()
	Setup.InitialRouter()
}
