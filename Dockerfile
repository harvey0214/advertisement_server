ARG GO_VERSION=1.17

FROM golang:${GO_VERSION}-alpine AS builder
#RUN apk update && apk add alpine-sdk git && rm -rf /var/cache/apk/*

# Configure Go
ENV GOPATH /go
RUN mkdir -p $GOPATH/src/pv_media_server
WORKDIR $GOPATH/src/pv_media_server
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY . .

RUN go build -o /app ./Sources/Run/main.go

FROM alpine:latest
#FROM gcr.io/distroless/base-debian10
#RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

COPY --from=builder /app .

ENTRYPOINT ["/app"]